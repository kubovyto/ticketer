# Ticketer application

### How to build

* To build application with maven framework, execute command 
    ```
        *mvn package*
    ```

### How to run

* To run application, execute command
    ```
        java -jar ticketer-server/target/Ticketer-exec.jar 
    ```
    
## Rest API
Address depends on environment, where the application is running on.

* Get actual ticket
    ```
        GET http://localhost:8080/tickets
    ```
* Create new ticket and add to queue
    ```
        POST http://localhost:8080/tickets 
    ```
* Delete actual ticket and remove from queue
    ```
        DELETE http://localhost:8080/tickets 
    ```