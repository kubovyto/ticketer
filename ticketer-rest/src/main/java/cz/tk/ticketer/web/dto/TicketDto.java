
package cz.tk.ticketer.web.dto;

import java.time.LocalDateTime;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * DTO ticket object.
 *
 * @author Tomáš Kubový
 */
public class TicketDto {
    private int number;
    private int order;
    private LocalDateTime created;

    public TicketDto(final int number, final int order, final LocalDateTime created) {
        this.number = number;
        this.order = order;
        this.created = created;
    }

    public int getNumber() {
        return number;
    }

    public int getOrder() {
        return order;
    }

    public LocalDateTime getCreated() {
        return created;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("number", number)
                .append("order", order)
                .append("created", created)
                .toString();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;

        if (!(o instanceof TicketDto)) return false;

        final TicketDto ticketDto = (TicketDto) o;

        return new EqualsBuilder()
                .append(number, ticketDto.number)
                .append(order, ticketDto.order)
                .append(created, ticketDto.created)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(number)
                .append(order)
                .append(created)
                .toHashCode();
    }
}
