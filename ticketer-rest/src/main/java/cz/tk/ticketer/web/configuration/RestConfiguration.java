
package cz.tk.ticketer.web.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration of web module.
 *
 * @author Tomáš Kubový
 */
@Configuration
@ComponentScan("cz.tk.ticketer.web")
public class RestConfiguration {

}
