
package cz.tk.ticketer.web.mapper;

import cz.tk.ticketer.core.domain.TicketOrdered;
import cz.tk.ticketer.web.dto.TicketDto;

/**
 * Mapper to map ticket objects.
 *
 * @author Tomáš Kubový
 */
public class TicketMapper {

    private static TicketMapper INSTANCE;

    private TicketMapper() {
    }


    public TicketDto toTicketDto(TicketOrdered ticket) {
        if (ticket == null) {
            return null;
        }

        return new TicketDto(ticket.getNumber(), ticket.getOrder(), ticket.getCreated());
    }


    public static TicketMapper getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new TicketMapper();
        }
        return INSTANCE;
    }

}
