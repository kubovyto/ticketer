
package cz.tk.ticketer.web.controller;

import java.util.Optional;

import cz.tk.ticketer.core.api.TicketService;
import cz.tk.ticketer.core.domain.TicketOrdered;
import cz.tk.ticketer.web.dto.TicketDto;
import cz.tk.ticketer.web.mapper.TicketMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Ticket controller. Manage operations upon tickets.
 *
 * @author Tomáš Kubový
 */
@RestController
@RequestMapping("/tickets")
public class TicketController {

    public static final Logger LOG = LoggerFactory.getLogger(TicketController.class);

    @Autowired
    private TicketService ticketService;


    /**
     * Gets actual ticket.
     *
     * @return 200 - actual ticket information, 404 - no one ticket exist
     */
    @GetMapping
    public ResponseEntity<TicketDto> getActualTicket() {
        LOG.debug("Getting actual ticket");

        Optional<TicketOrdered> actualTicket = ticketService.findActualTicket();
        if (actualTicket.isPresent()) {
            return new ResponseEntity<>(TicketMapper.getInstance().toTicketDto(actualTicket.get()), HttpStatus.OK);
        } else {
            LOG.debug("No active ticket in queue.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Create new ticket.
     *
     * @return 201 - ticket information
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TicketDto createTicket() {
        TicketOrdered ticket = ticketService.createTicket();
        return TicketMapper.getInstance().toTicketDto(ticket);
    }

    /**
     * Remove active ticket.
     * 204 success
     */
    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeActiveTicket() {
        ticketService.removeActualTicket();
    }
}

