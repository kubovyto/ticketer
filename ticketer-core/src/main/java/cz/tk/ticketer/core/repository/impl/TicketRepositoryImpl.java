
package cz.tk.ticketer.core.repository.impl;

import java.util.LinkedList;
import java.util.Optional;
import javax.annotation.PostConstruct;

import cz.tk.ticketer.core.domain.Ticket;
import cz.tk.ticketer.core.repository.TicketRepository;
import org.springframework.stereotype.Repository;

/**
 * Ticket repository. Managing operations with tickets.
 *
 * @author Tomáš Kubový
 */
@Repository
public class TicketRepositoryImpl implements TicketRepository {

    private LinkedList<Ticket> ticketList;


    @Override
    public Optional<Ticket> findActualTicket() {
        return Optional.ofNullable(ticketList.peekLast());
    }

    @Override
    synchronized public int pushTicket(final Ticket ticket) {
        ticketList.push(ticket);
        return ticketList.size();
    }

    @Override
    synchronized public void pollActualTicket() {
        ticketList.pollLast();
    }

    @PostConstruct
    private void init() {
        ticketList = new LinkedList<>();
    }
}
