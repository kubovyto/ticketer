
package cz.tk.ticketer.core.service;

import java.time.LocalDateTime;
import java.util.Optional;

import cz.tk.ticketer.core.api.TicketService;
import cz.tk.ticketer.core.domain.Ticket;
import cz.tk.ticketer.core.domain.TicketOrdered;
import cz.tk.ticketer.core.repository.TicketNumberGenerator;
import cz.tk.ticketer.core.repository.TicketRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementation of {@link TicketService}.
 *
 * @author Tomáš Kubový
 */
@Service
public class TicketServiceImpl implements TicketService {

    private static final Logger LOG = LoggerFactory.getLogger(TicketServiceImpl.class);

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private TicketNumberGenerator ticketNumberGenerator;


    @Override
    synchronized public TicketOrdered createTicket() {
        LOG.debug("Creating new ticket.");
        final Integer ticketNumber = ticketNumberGenerator.nextNumber();
        final Ticket ticket = new Ticket(ticketNumber, LocalDateTime.now());

        int size = ticketRepository.pushTicket(ticket);
        final TicketOrdered result = new TicketOrdered(ticketNumber, ticket.getCreated(), size - 1);

        LOG.debug("Created ticket {}", result);
        return result;
    }

    @Override
    public Optional<TicketOrdered> findActualTicket() {
        LOG.debug("Finding actual ticket. (The oldest one)");

        Optional<Ticket> actualTicket = ticketRepository.findActualTicket();
        if (actualTicket.isPresent()) {
            return Optional.of(toTicketOrdered(actualTicket.get(), 0));
        }

        return Optional.empty();
    }

    @Override
    public void removeActualTicket() {
        LOG.debug("Removing actual ticket.");
        ticketRepository.pollActualTicket();
    }


    private TicketOrdered toTicketOrdered(final Ticket ticket, final int order) {
        if (ticket == null) {
            return null;
        }
        return new TicketOrdered(ticket.getNumber(), ticket.getCreated(), order);
    }
}