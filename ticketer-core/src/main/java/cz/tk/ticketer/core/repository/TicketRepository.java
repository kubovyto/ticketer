
package cz.tk.ticketer.core.repository;

import java.util.Optional;
import javax.validation.constraints.NotNull;

import cz.tk.ticketer.core.domain.Ticket;
import org.springframework.validation.annotation.Validated;

/**
 * Ticket repository managing operations upon tickets.
 *
 * @author Tomáš Kubový
 */
@Validated
public interface TicketRepository {

    /**
     * Push ticket to repository.
     *
     * @param ticket ticket to be pushed
     * @return the number of tickets stored in repository
     */
    int pushTicket(@NotNull Ticket ticket);

    /**
     * Poll actual (the oldest one) ticket from repository.
     */
    void pollActualTicket();

    /**
     * Finds actual (the oldest one) ticket from repository.
     *
     * @return optional of actual ticket
     */
    @NotNull
    Optional<Ticket> findActualTicket();
}
