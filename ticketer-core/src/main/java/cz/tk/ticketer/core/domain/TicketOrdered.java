
package cz.tk.ticketer.core.domain;

import java.time.LocalDateTime;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Ticket domain object with order information.
 *
 * @author Tomáš Kubový
 */
public class TicketOrdered extends Ticket {

    private int order;

    public TicketOrdered(final Integer number, final LocalDateTime created, final int order) {
        super(number, created);
        this.order = order;
    }

    public int getOrder() {
        return order;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .appendSuper(super.toString())
                .append("order", order)
                .append("number", getNumber())
                .append("created", getCreated())
                .toString();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;

        if (!(o instanceof TicketOrdered)) return false;

        final TicketOrdered that = (TicketOrdered) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(order, that.order)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(order)
                .toHashCode();
    }
}
