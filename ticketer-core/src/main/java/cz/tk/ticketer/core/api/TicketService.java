
package cz.tk.ticketer.core.api;

import java.util.Optional;
import javax.validation.constraints.NotNull;

import cz.tk.ticketer.core.domain.TicketOrdered;
import org.springframework.validation.annotation.Validated;

/**
 * Ticket service api.
 *
 * @author Tomáš Kubový
 */
@Validated
public interface TicketService {

    /**
     * Creates new ticket.
     *
     * @return created ticket.
     */
    @NotNull
    TicketOrdered createTicket();

    /**
     * Finds actual ticket in repository.
     *
     * @return optional of actual ticket
     */
    @NotNull
    Optional<TicketOrdered> findActualTicket();

    /**
     * Removes actual ticket from repository.
     */
    void removeActualTicket();
}
