
package cz.tk.ticketer.core.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Core configuration.
 *
 * @author Tomáš Kubový
 */
@Configuration
@ComponentScan("cz.tk.ticketer.core")
public class CoreConfiguration {

}
