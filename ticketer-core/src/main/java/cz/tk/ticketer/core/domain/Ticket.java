
package cz.tk.ticketer.core.domain;

import java.time.LocalDateTime;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Ticket domain object.
 *
 * @author Tomáš Kubový
 */
public class Ticket {
    private Integer number;
    private LocalDateTime created;

    public Ticket(final Integer number, final LocalDateTime created) {
        this.number = number;
        this.created = created;
    }

    public Integer getNumber() {
        return number;
    }


    public LocalDateTime getCreated() {
        return created;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("number", number)
                .append("created", created)
                .toString();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;

        if (!(o instanceof Ticket)) return false;

        final Ticket ticket = (Ticket) o;

        return new EqualsBuilder()
                .append(number, ticket.number)
                .append(created, ticket.created)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(number)
                .append(created)
                .toHashCode();
    }
}
