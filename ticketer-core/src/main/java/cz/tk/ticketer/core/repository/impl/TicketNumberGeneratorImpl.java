
package cz.tk.ticketer.core.repository.impl;

import javax.annotation.PostConstruct;

import cz.tk.ticketer.core.repository.TicketNumberGenerator;
import org.springframework.stereotype.Repository;

/**
 * Implementation of number generator repository.
 *
 * @author Tomáš Kubový
 */
@Repository
public class TicketNumberGeneratorImpl implements TicketNumberGenerator {

    public static final int INIT_NUMBER = 1000;
    public static final int MAX_NUMBER = 9999;

    private int counter;

    @Override
    public Integer nextNumber() {
        if (counter == MAX_NUMBER) {
            counter = INIT_NUMBER;
            return counter;
        }

        return ++counter;
    }

    @PostConstruct
    private void init() {
        counter = INIT_NUMBER;
    }
}
