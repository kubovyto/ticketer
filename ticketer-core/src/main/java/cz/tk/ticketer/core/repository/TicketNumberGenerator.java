
package cz.tk.ticketer.core.repository;

/**
 * Number generator.
 *
 * @author Tomáš Kubový
 */
public interface TicketNumberGenerator {

    /**
     * Generates next number.
     *
     * @return generated number
     */
    Integer nextNumber();
}
