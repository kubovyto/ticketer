package cz.tk.ticketer.core.service;

import java.util.Optional;

import cz.tk.ticketer.core.api.TicketService;
import cz.tk.ticketer.core.configuration.CoreConfiguration;
import cz.tk.ticketer.core.domain.TicketOrdered;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Test suites for {@link TicketServiceImpl}.
 *
 * @author Tomáš Kubový
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = CoreConfiguration.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class TicketServiceImplTest {


    @Autowired
    private TicketService ticketService;

    @Test
    public void createTicket() throws Exception {
        TicketOrdered ticket = ticketService.createTicket();
        Assert.assertNotNull(ticket);
        Assert.assertEquals(ticket.getOrder(), 0);
    }

    @Test
    public void findActualTicket() throws Exception {
        Optional<TicketOrdered> actualTicket = ticketService.findActualTicket();
        Assert.assertFalse(actualTicket.isPresent());

        TicketOrdered createdTicket = ticketService.createTicket();

        Optional<TicketOrdered> actualTicketFound = ticketService.findActualTicket();
        Assert.assertTrue(actualTicketFound.isPresent());
        Assert.assertEquals(createdTicket, actualTicketFound.get());
    }

    @Test
    public void removeActualTicket() throws Exception {
        ticketService.createTicket();
        Assert.assertTrue(ticketService.findActualTicket().isPresent());

        ticketService.removeActualTicket();
        Assert.assertFalse(ticketService.findActualTicket().isPresent());
    }
}