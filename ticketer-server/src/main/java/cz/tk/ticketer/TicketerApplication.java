
package cz.tk.ticketer;

import cz.tk.ticketer.core.configuration.CoreConfiguration;
import cz.tk.ticketer.web.configuration.RestConfiguration;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;
import org.springframework.util.Assert;

/**
 * @author Tomáš Kubový
 */
@SpringBootApplication
@Import({CoreConfiguration.class, RestConfiguration.class})
public class TicketerApplication extends SpringBootServletInitializer {

    /**
     * Start application.
     *
     * @param args arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(TicketerApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        Assert.notNull(application, "application must not be null");

        return application.bannerMode(Banner.Mode.CONSOLE)
                          .sources(TicketerApplication.class);
    }
}
